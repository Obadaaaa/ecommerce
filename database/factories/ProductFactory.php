<?php

namespace Database\Factories;

use App\Models\category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->sentence(),
            'description'=>$this->faker->paragraph(),
            'image'=>$this->faker->image('public/images',640,480),
            'category'=>category::all()->random(),
            'quantity'=>$this->faker->numberBetween(1000,10000),
            'price'=>$this->faker->numberBetween(1000,10000),
            'discount_price'=>$this->faker->numberBetween(1000,10000),
        ];
    }
}
