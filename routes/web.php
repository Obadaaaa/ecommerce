<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = Product::paginate(3);
    return view('home.userpage',compact('products'));
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class,'index'])->name('home');

Route::get('/products',[App\Http\Controllers\HomeController::class,'show_products']);

Route::get('/products/details/{id}',[App\Http\Controllers\HomeController::class,'details']);

Route::post('add_cart/{id}',[App\Http\Controllers\HomeController::class,'add_cart']);
Route::get('showCart',[App\Http\Controllers\HomeController::class,'showCart']);

Route::get('cartRemove/{id}',[App\Http\Controllers\HomeController::class,'deletecart']);

Route::get('/cart/cashorder',[App\Http\Controllers\HomeController::class,'cashorder']);

Route::get('/products_search',[App\Http\Controllers\HomeController::class,'search_product']);

Route::middleware('auth')->group(function(){

    Route::prefix('order')->group(function(){
    Route::get('/show',[App\Http\Controllers\HomeController::class ,'ShowOrder']);
    Route::put('/cancel/{id}',[App\Http\Controllers\HomeController::class ,'cancel_order']);
    });

});







//admin
Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('/',[App\Http\Controllers\AdminController::class,'index']);
//Categoreis
    Route::prefix('Categories')->group(function(){
    Route::get('/',[App\Http\Controllers\AdminCategory::class,'index']);
    Route::post('/create',[App\Http\Controllers\AdminCategory::class,'create']);
    Route::get('/{id}/edit',[App\Http\Controllers\AdminCategory::class,'edit']);
    Route::put('/{id}',[App\Http\Controllers\AdminCategory::class,'update']);
    Route::delete('/{id}',[App\Http\Controllers\AdminCategory::class,'destory']);
});
//Prodcuts
    Route::prefix('Products')->group(function(){
    Route::get('/',[App\Http\Controllers\AdminProducts::class,'index']);
    Route::get('/create',[App\Http\Controllers\AdminProducts::class,'create']);
    Route::post('/store',[App\Http\Controllers\AdminProducts::class,'store']);
    Route::get('/{id}/edit',[App\Http\Controllers\AdminProducts::class,'edit']);
    Route::delete('/{id}',[App\Http\Controllers\AdminProducts::class,'destroy']);
    Route::put('/{id}',[App\Http\Controllers\AdminProducts::class,'update']);
});
//Orders
Route::get('/Orders',[App\Http\Controllers\AdminOrder::class,'index']);

Route::get('/Orders/deliverd/{id}',[App\Http\Controllers\AdminOrder::class,'deliverd']);
Route::get('/Orders/printPDF/{id}',[App\Http\Controllers\AdminOrder::class,'printPdf']);
Route::get('/Orders/search',[App\Http\Controllers\AdminOrder::class,'searchdata']);


});

    



