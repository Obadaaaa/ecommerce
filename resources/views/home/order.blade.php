

<!DOCTYPE html>
<html>
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="{{ asset('DashBoard/images/favicon.png') }}" type="">
      <title>Famms - Fashion HTML Template</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.main.css') }}" />
      <!-- font awesome style -->
      <link href="{{ asset('DashBoard/css/font-awesome.min.css') }}" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="{{ asset('DashBoard/css/style.css') }}" rel="stylesheet" />
      <!-- responsive style -->
      <link href="{{asset('DashBoard/css/responsive.css') }}" rel="stylesheet" />
      <style type="text/css">
        .center{
            margin: auto;
            width: 70%;
            padding: 30px;

        }
        table,th,td{
            border: 1px solid black;
        }
        .th_deg{
            padding: 10px;
            background-color: skyblue;
            font-size: 20px;
            font-weight: bold;
        }
        </style>
   </head>
   <body>

         <!-- header section strats -->
         @include('home.header')
         <!-- end header section -->
         <!-- slider section -->

         <!-- end slider section -->
  
      <div class="center">
        <table>
             <thead class="th_deg">
                <tr>
<th>Poduct Title </th>
<th> Quantity</th>
<th> Price </th>
<th> Payment Status</th>
<th> Delivary Status </th>
<th> Image </th>
<th> Cancel Order </th>


                </tr>
             </thead>
             @foreach ($order as $order )
                 
    
             <tbody>
                <tr>
                    <td> {{ $order->products->title }}</td>
                    <td> {{ $order->quantity }}</td>
                    <td> {{ $order->price }}</td>
                 
                    <td> {{ $order->payment_status }}</td>
                    <td> {{ $order->delivery_status }}</td>
<td>
<img src="/images/{{ $order->products->image }}" alt="" height="200px" width="200px">
</td>
<td>
    @if($order->delivery_status =='processing')
    <form action="/order/cancel/{{ $order->id }}" method="POST" >
        @csrf
        @method("PUT")
        <input type="submit" class="btn btn-danger" value="Cancel Order">
        @else
        <p> Done </p>
        @endif
    </form>
   
   

</td>
                </tr>

             </tbody>
             @endforeach
        </table>
      </div>
     
      <!-- jQery -->
      <script src="{{ asset('assets/js/jquery.js') }}" defer></script>
      <!-- popper js -->
      <script src="{{ asset('DashBoard/js/popper.min.js') }}" defer></script>
      <!-- bootstrap js -->
      <script src="{{ asset('assets/js/bootstrap.main.js') }}" defer></script>
      <!-- custom js -->
      <script src="{{ asset('DashBoard/js/custom.js') }}" defer></script>
   </body>
</html>