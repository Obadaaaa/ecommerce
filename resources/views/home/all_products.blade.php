

<!DOCTYPE html>
<html>
   <head>
      <!-- Basic -->
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="{{ asset('DashBoard/images/favicon.png') }}" type="">
      <title>Famms - Fashion HTML Template</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.main.css') }}" />
      <!-- font awesome style -->
      <link href="{{ asset('DashBoard/css/font-awesome.min.css') }}" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="{{ asset('DashBoard/css/style.css') }}" rel="stylesheet" />
      <!-- responsive style -->
      <link href="{{asset('DashBoard/css/responsive.css') }}" rel="stylesheet" />
   </head>
   <body>
      <div class="hero_area">
         <!-- header section strats -->
         @include('home.header')
         <!-- end header section -->
        
      <!-- end why section -->
      
      <!-- arrival section -->
      
      <!-- end arrival section -->
      
      <!-- product section -->
      @include('home.product')
      <!-- end product section -->

      <!-- subscribe section -->
    
      <!-- jQery -->
      <script src="{{ asset('assets/js/jquery.js') }}" defer></script>
      <!-- popper js -->
      <script src="{{ asset('DashBoard/js/popper.min.js') }}" defer></script>
      <!-- bootstrap js -->
      <script src="{{ asset('assets/js/bootstrap.main.js') }}" defer></script>
      <!-- custom js -->
      <script src="{{ asset('DashBoard/js/custom.js') }}" defer></script>
   </body>
</html>