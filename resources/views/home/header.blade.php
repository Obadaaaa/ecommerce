<header class="header_section">
    <div class="container">
       <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="/"><img width="250" src="DashBoard/images/logo.png" alt="#" /></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class=""> </span>
          </button>
          @if(Route::has('login'))
          @auth
          <a href="/admin"> AdminPage</a>
          @endauth
          @endif
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
             <ul class="navbar-nav">
                <li class="nav-item active">
                   <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
               
                <li class="nav-item">
                   <a class="nav-link" href="{{ url('/products') }}">Products</a>
                   @csrf
                </li>
                

                
                <li class="nav-item">
                  <a class="nav-link" href=" {{url('/showCart')}}">Cart</a>
               </li>

              
               <li class="nav-item">
                  <a class="nav-link" href="{{url('/order/show')}}">Order</a>
               </li>

                @if(Route::has('login'))
                @auth
                    
                <li class="nav-item">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

               
                        

                        <form id="logout-form" action="{{route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                    




                    
                 </li>
              




                 
                @else
                <li class="nav-item">
                    <a class="btn btn-primary"  id ='logincss' href="{{route('login')}}">Log in</a>
                 </li>

                 <li class="nav-item">
                    <a class="btn btn-success" href="{{ route('register') }}">Register</a>
                 </li>
                 @endauth 
                @endif










                <form class="form-inline">
                   <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                   <i class="fa fa-search" aria-hidden="true"></i>
                   </button>
                </form>
             </ul>
          </div>
       </nav>
    </div>
 </header>