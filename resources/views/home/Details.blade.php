

<!DOCTYPE html>
<html>
   <head>
      <!-- Basic -->
      <base href="/public">
            <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Site Metas -->
      <meta name="keywords" content="" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <link rel="shortcut icon" href="{{ asset('DashBoard/images/favicon.png') }}" type="">
      <title>Famms - Fashion HTML Template</title>
      <!-- bootstrap core css -->
      <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.main.css') }}" />
      <!-- font awesome style -->
      <link href="{{ asset('DashBoard/css/font-awesome.min.css') }}" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="{{ asset('DashBoard/css/style.css') }}" rel="stylesheet" />
      <!-- responsive style -->
      <link href="{{asset('DashBoard/css/responsive.css') }}" rel="stylesheet" />
   </head>
   <body>

         <!-- header section strats -->
         @include('home.header')
         <!-- end header section -->
         <!-- slider section -->
         {{ $product->name }}
         
         <!-- end slider section -->
     
      <div>
        

  
                <div class="col-sm-6 col-md-4 col-lg-4" style="margin:auto; width:50%;  "  >
                 
               <div class="img-box">
                  <img src="images/{{$product->image }}" alt="" width="200px" height="200px">
               </div>
               <div class="detail-box">
                  <h5>
                     {{ $product->title }}
                  </h5>
                  @if($product->discount_price!=null)
                  <h6  style="color: red">
                    Discount Price :
                    {{ $product->discount_price }}$
                     </h6>
                     <h6 style="text-decoration:line-through; color:blue;">
                       Price : 
                       {{ $product->price }}$
                        </h6>
                    @else
                  <h6 style=" color:blue;">
                    Price
                    <br>

                 {{ $product->price }}$
                  </h6>
                  @endif
                  <h6 >Product Category : {{ $product->category }}</h6>
                  <h6> Product Details : {{ $product->description }}</h6>
                  <h6>Availabe : {{ $product->quantity }}</h6>

                  <form action="{{url('add_cart/'.$product->id) }}" method="POST">
                     @csrf
                     <div class="row">
                        <div class="col-md-4">
   
                           <input type="number" name="quantity" value ="1" min ="1" style="width:100px ;">
                        </div>
                        <div class ="col-md-4">
                           <input type ="submit" value="Add to cart">
                        </div>
                       </div>
                  </form>
                  
                </div>

              </div>




      <footer>
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                   <div class="full">
                      <div class="logo_footer">
                        <a href="#"><img width="210" src="DashBoard/images/logo.png" alt="#" /></a>
                      </div>
                      <div class="information_f">
                        <p><strong>ADDRESS:</strong> 28 White tower, Street Name New York City, USA</p>
                        <p><strong>TELEPHONE:</strong> +91 987 654 3210</p>
                        <p><strong>EMAIL:</strong> yourmain@gmail.com</p>
                      </div>
                   </div>
               </div>
               <div class="col-md-8">
                  <div class="row">
                  <div class="col-md-7">
                     <div class="row">
                        <div class="col-md-6">
                     <div class="widget_menu">
                        <h3>Menu</h3>
                        <ul>
                           <li><a href="#">Home</a></li>
                           <li><a href="#">About</a></li>
                           <li><a href="#">Services</a></li>
                           <li><a href="#">Testimonial</a></li>
                           <li><a href="#">Blog</a></li>
                           <li><a href="#">Contact</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="widget_menu">
                        <h3>Account</h3>
                        <ul>
                           <li><a href="#">Account</a></li>
                           <li><a href="#">Checkout</a></li>
                           <li><a href="#">Login</a></li>
                           <li><a href="#">Register</a></li>
                           <li><a href="#">Shopping</a></li>
                           <li><a href="#">Widget</a></li>
                        </ul>
                     </div>
                  </div>
                     </div>
                  </div>     
                  <div class="col-md-5">
                     <div class="widget_menu">
                        <h3>Newsletter</h3>
                        <div class="information_f">
                          <p>Subscribe by our newsletter and get update protidin.</p>
                        </div>
                        <div class="form_sub">
                           <form>
                              <fieldset>
                                 <div class="field">
                                    <input type="email" placeholder="Enter Your Mail" name="email" />
                                    <input type="submit" value="Subscribe" />
                                 </div>
                              </fieldset>
                           </form>
                        </div>
                     </div>
                  </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- footer end -->
      <div class="cpy_">
         <p class="mx-auto">© 2021 All Rights Reserved By <a href="https://html.design/">Free Html Templates</a><br>
         
            Distributed By <a href="https://themewagon.com/" target="_blank">ThemeWagon</a>
         
         </p>
      </div>
      <!-- jQery -->
      <script src="{{ asset('assets/js/jquery.js') }}" defer></script>
      <!-- popper js -->
      <script src="{{ asset('DashBoard/js/popper.min.js') }}" defer></script>
      <!-- bootstrap js -->
      <script src="{{ asset('assets/js/bootstrap.main.js') }}" defer></script>
      <!-- custom js -->
      <script src="{{ asset('DashBoard/js/custom.js') }}" defer></script>
   </body>
</html>3:11 PM 7/23/2023