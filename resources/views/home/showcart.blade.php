<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="{{ asset('DashBoard/images/favicon.png') }}" type="">
    <title>Famms - Fashion HTML Template</title>
    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.main.css') }}" />
    <!-- font awesome style -->
    <link href="{{ asset('DashBoard/css/font-awesome.min.css') }}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ asset('DashBoard/css/style.css') }}" rel="stylesheet" />
    <!-- responsive style -->
    <link href="{{asset('DashBoard/css/responsive.css') }}" rel="stylesheet" />
    <style type="text/css" >
    .center{
        margin: auto;
        width: 50%;
        text-align: center;
        padding: 30px;
    }
    table ,th ,td{
        border: 1px solid gray;

    }
    .th_deg{
        font-size: 30px;
        padding: 5px;
        background: skyblue;
             
    }
    .img_deg{
        height: 200px;
        width: 200px;
    }
    .total_deg{
        font-size: 20px;
        padding: 40px;
     position: absolute;
     right:50%
    }
    </style>
</head>
<body>
    <div class="hero_area">
        <!-- header section strats -->
        @include('home.header')
        <!-- end header section -->
        <!-- slider section -->

        @if (session()->has('message'))
        <div class="alert alert-success">
        
            {{ session()->get('message') }}
        </div>
        @endif

    <div>
        <table class ="center">
            <thead>
<tr>
<th class ="th_deg">Product title </th>
<th class ="th_deg">Product quantity</th>
<th class ="th_deg"> price </th>
<th class ="th_deg">Image </th>
<th class ="th_deg">Action</th>

</tr>

            </thead>

            <?php $totalprice= 0 ?>
@foreach ($cart as $ca )
    

            <tbody>
                <tr>
<td>{{ $ca->products->title}} </td>
<td> {{ $ca->quantity }}</td>
<td>  {{ $ca->price }}$</td>


<td>
<img class="img_deg" src="images/{{ $ca->products->image }}" >

</td>
<td> <a  class = "btn btn-danger" onclick="return confirm('Are You sure to remove this product?')"
    href="/cartRemove/{{ $ca->id }}" >Remove</a></td>




<?php $totalprice = $totalprice + $ca->price  ?>
</tr>
            </tbody>
            @endforeach
        </table>
        <div class="total_deg">
         <h1 > Total Price :  {{ $totalprice }}$  </h1>  

        </div>
        <br>
<br><br><br><br>
<div style=" position: absolute;
right:50%">
    <h1> Proceed to Order 
    </h1>
    <a href="{{ url('/cart/cashorder') }}" class="btn btn-danger"> Cash On Delivary</a>
    
</div>
<br><br><br>
    </div>
        <!-- end slider section -->
     </div>

     <!-- why section -->
   
     @include('home.why')
     <!-- end why section -->
     
     <!-- arrival section -->
     
     <!-- end arrival section -->
     
     <!-- product section -->

     <!-- end product section -->

     <!-- subscribe section -->
    @include('home.subscribe')
     <!-- end subscribe section -->
     <!-- client section -->
    @include('home.client')
     <!-- end client section -->
     <!-- footer start -->
     <footer>
        <div class="container">
           <div class="row">
              <div class="col-md-4">
                  <div class="full">
                     <div class="logo_footer">
                       <a href="#"><img width="210" src="DashBoard/images/logo.png" alt="#" /></a>
                     </div>
                     <div class="information_f">
                       <p><strong>ADDRESS:</strong> 28 White tower, Street Name New York City, USA</p>
                       <p><strong>TELEPHONE:</strong> +91 987 654 3210</p>
                       <p><strong>EMAIL:</strong> yourmain@gmail.com</p>
                     </div>
                  </div>
              </div>
              <div class="col-md-8">
                 <div class="row">
                 <div class="col-md-7">
                    <div class="row">
                       <div class="col-md-6">
                    <div class="widget_menu">
                       <h3>Menu</h3>
                       <ul>
                          <li><a href="#">Home</a></li>
                          <li><a href="#">About</a></li>
                          <li><a href="#">Services</a></li>
                          <li><a href="#">Testimonial</a></li>
                          <li><a href="#">Blog</a></li>
                          <li><a href="#">Contact</a></li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-md-6">
                    <div class="widget_menu">
                       <h3>Account</h3>
                       <ul>
                          <li><a href="#">Account</a></li>
                          <li><a href="#">Checkout</a></li>
                          <li><a href="#">Login</a></li>
                          <li><a href="#">Register</a></li>
                          <li><a href="#">Shopping</a></li>
                          <li><a href="#">Widget</a></li>
                       </ul>
                    </div>
                 </div>
                    </div>
                 </div>     
                 <div class="col-md-5">
                    <div class="widget_menu">
                       <h3>Newsletter</h3>
                       <div class="information_f">
                         <p>Subscribe by our newsletter and get update protidin.</p>
                       </div>
                       <div class="form_sub">
                          <form>
                             <fieldset>
                                <div class="field">
                                   <input type="email" placeholder="Enter Your Mail" name="email" />
                                   <input type="submit" value="Subscribe" />
                                </div>
                             </fieldset>
                          </form>
                       </div>
                    </div>
                 </div>
                 </div>
              </div>
           </div>
        </div>
     </footer>
     <!-- footer end -->
     <div class="cpy_">
        <p class="mx-auto">© 2021 All Rights Reserved By <a href="https://html.design/">Free Html Templates</a><br>
        
           Distributed By <a href="https://themewagon.com/" target="_blank">ThemeWagon</a>
        
        </p>
     </div>
     <!-- jQery -->
     <script src="{{ asset('assets/js/jquery.js') }}" defer></script>
     <!-- popper js -->
     <script src="{{ asset('DashBoard/js/popper.min.js') }}" defer></script>
     <!-- bootstrap js -->
     <script src="{{ asset('assets/js/bootstrap.main.js') }}" defer></script>
     <!-- custom js -->
     <script src="{{ asset('DashBoard/js/custom.js') }}" defer></script>
</body>
</html>