<nav class="navbar p-0 fixed-top d-flex flex-row">
   
    <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
      
    
      <ul class="navbar-nav navbar-nav-right">
        
        
       
       
        <li class="nav-item dropdown border-left">
          <a class="nav-link count-indicator dropdown-toggle" id="Logout_btn" href="#" data-toggle="dropdown">
            {{ Auth::user()->name }}
          </a>

          <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="Logout_btn">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
         </li>

        






        
       
      </ul>
     
 
    </div>
  
  </nav>
  