<!DOCTYPE html>
<html lang="en">
<head>
@include('admin.css')

<style>
    .th_deg{
        background: skyblue;
    }
    .table_deg{
        border: 2px solid white;
        width: 100%;
        margin: auto;
padding-top: 50px;
text-align: center;
    }

    </style>
   
</head>
<body>
    <div class="container-scroller">
        @include('admin.sidebar')
        @include('admin.navbar');

<div class='main-panel'>
    <div class="content-wrapper">
<h1 class ="title_deg"> All Orders </h1>
<div style="padding-left: 400px; padding-bottom: 30px ">
    <form action="{{ url('admin/Orders/search') }}" method="GET">
        <input type ="text" name="search" placeholder="Search For Something">
        <input type="submit" value="Search" class="btn btn-outline-primary">
    </form>
</div>
<table class ="table_deg">
    <thead class="th_deg">
        <tr>
    <th style="padding: 10px;">  Name </th>
    <th  style="padding: 10px;">  Email </th>
    <th  style="padding: 10px;">  Address </th>
    <th  style="padding: 10px;">  Phone </th>
    <th  style="padding: 10px;">  Product Title </th>
    <th  style="padding: 10px;">  Quantity </th>
    <th  style="padding: 10px;" >  Price </th>
    <th  style="padding: 10px;">  Payment Status </th>
    <th  style="padding: 10px;">  Deleviry Status </th>
    <th  style="padding: 10px;">  Image </th>
    <th  style="padding: 10px;"> Deliverd </th>
    <th style="padding: 10px;"> PDF </th>

        </tr>
    </thead>
    @foreach ($orders as $order )
        
  
    <tbody>
        <tr>
        <td> {{ $order->users->name }}</td>
        <td> {{ $order->users->email }}</td>
        <td> {{ $order->users->address }}</td>
        <td> {{ $order->users->phone }}</td>
        <td> {{ $order->products->title }}</td>
        <td> {{ $order->quantity }}</td>
        <td> {{ $order->price }}</td>
        <td> {{ $order->payment_status }}</td>
        <td> {{ $order->delivery_status  }}</td>
        <td> <img src="/images/{{ $order->products->image  }}" width="50px" height="50px"></td>
  <td>
    @if ($order->delivery_status == "processing")

    <a href="{{ url('/admin/Orders/deliverd/'.$order->id) }}" class ="btn btn-primary"> Deliverd</a>
    @else
    <p> Deliverd </p>
    @endif  

</td>
<td>
    <a href="{{ url('/admin/Orders/printPDF/'.$order->id) }}" class ="btn btn-secondary"> Print</a>
    
</td>

 

            

        </tr>

    </tbody>
    @endforeach
</table>
    </div>
</div>



        @include('admin.script');
    </div>
    
</body>
</html>