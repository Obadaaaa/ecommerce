<!DOCTYPE html>
<html lang="en">
<head>
  
  @include('admin.css')
  <style type="text/css">
  .div_center{
    text-align: center;
    padding-top:40px ; }
  </style>
</head>
<body>
  <div class ="container-scroller">
  @include('admin.navbar')
  @include('admin.sidebar')



  <!--  Ok !-->
@if (session()->has('message'))
<div class="alert alert-success">

    {{ session()->get('message') }}
</div>
@endif
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{url('/admin/Products/store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label for="title" class="col-md-4 col-form-label text-md-end">Title :</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="title" required autocomplete="name" autofocus>

                               
                            </div>
                        </div>



                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Description :</label>

                            <div class="col-md-6">
                                <textarea class="from-contol" name ="description" required> </textarea>
                     

                             
                            </div>
                        </div>



                        <div class="row mb-3">
                            <label for="price" class="col-md-4 col-form-label text-md-end">Price :</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" required >

                             
                            </div>
                        </div>




                        <div class="row mb-3">
                            <label for="discount" class="col-md-4 col-form-label text-md-end">Discount Price :</label>

                            <div class="col-md-6">
                                <input id="discount" type="text" class="form-control" name="discount"  >
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">Product Quantity :</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="quantity"  required>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-md-4 col-form-label text-md-end">Category :</label>
                            <select class="text_color" name ="category">
                                <option value="" selected=""> Add Category Here</option>
                                @foreach ($category as $ca)
                                    
                               
                                <option> {{  $ca->name}} </option>
                                @endforeach
                            </select>

                        </div>

                        <div class="row_mb-3">
                            <label> Product Image Here : </label>
                            <input type ="file" name="image">
                        </div>

                        
                   


                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                               Create
                                </button>



                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  
  <!--  Ok !-->
  @include('admin.script')
  </div>
</body>
</html>







