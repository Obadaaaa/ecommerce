<!DOCTYPE html>
<html lang="en">
<head>
  
  @include('admin.css')
</head>
<body>
  <div class ="container-scroller">
  @include('admin.navbar')
  @include('admin.sidebar')
 <div  class= "main-panel">
    <div class="content-wrapper">


        <table class="table table-bordered table-striped">
            <thead>
                <tr>
            <th> ID </th>
            <th> Title </th>
            <th> Description </th>
            <th> Image</th>
            <th> Category </th>
            <th> Quantity </th>
            <th> Price </th>
            <th> Discount Price </th>
            <th> Edit </th>
           
                </tr>
                
        </thead>
<tbody>
    <tr>
    @foreach ($products as $product )
  <td>  {{ $product->id }}</td>
  <td>  {{ $product->title }}</td>
  <td>  {{ $product->description }}</td>

  <td>  <img  src="/images/{{$product->image ? $product->image : 'http://placehold.it/200x200'}}" width="200" height="200"> </td>
  <td>  {{ $product->category }}</td>
  <td>  {{ $product->quantity }}</td>
  <td>  {{ $product->price }}</td>
  <td>  {{ $product->discount_price }}</td>
  <td> 
    <a href="{{url('admin/Products/'.$product->id.'/edit')}}" class="btn btn-success"> Edit</a>
       

    
        <form action="{{url('/admin/Products/'.$product->id)}}" method="POST">
          @method('DELETE')
          @csrf
          <button type="submit" class="btn btn-danger" onclick="return confirm('are u sure?')">Delete</button>               
         </form>


  </td>


   

  
    </tr>
</tbody>
@endforeach


        </table>
       



    </div>
 </div>



  @include('admin.script')
  </div>
</body>
</html>







