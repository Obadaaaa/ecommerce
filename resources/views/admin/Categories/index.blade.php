<!DOCTYPE html>
<html lang="en">
<head>
  
  @include('admin.css')
  <style type="text/css">
  .div_center{
    text-align: center;
    padding-top:40px ; }
  </style>
</head>
<body>
  <div class ="container-scroller">
  @include('admin.navbar')
  @include('admin.sidebar')
 <div class = "main-panel">
    <div class ="content-wrapper">
        
        @if(session()->has('message'))
        <div class="alert alert-success">
        {{ session()->get('message') }}
        </div>
        @endif

        @if(session()->has('deleted'))

        <div class="alert alert-success">
        {{ session()->get('deleted') }}
        </div>

        @endif

        <div class ="div_center">
            <h2> Add Category </h2>
<form action ="{{ url('admin/Categories/create') }}" method ="POST"> 
    @csrf   
    <input type="text" name="name" placeholder="Type Category Here">
    <input type = "submit" name="sumbit" class = "btn btn-primary" value="Add Category">
</form>

        </div>
        @csrf
 
            
    
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
            <th> ID </th>
            <th> Name </th>
            <th> Created_At </th>
            <th> Updated_At</th>
            <th> Action </th>
                </tr>
                
        </thead>
<tbody>
    @foreach ($category as $ca )    
    <td> {{$ca->id  }}</td>
    <td> {{$ca->name  }}</td>
    <td> {{$ca->created_at->diffForHumans() }}</td>
    <td> {{$ca->updated_at->diffForHumans()  }}</td>
    <td> 
      
        <a href="{{url('admin/Categories/'.$ca->id.'/edit')}}" class="btn btn-success"> Edit</a>
       
        <form action="{{url('/admin/Categories/'.$ca->id)}}" method="POST">
          @method('DELETE')
          @csrf
          <button type="submit" class="btn btn-danger">Delete</button>               
         </form>
      
    </td>
  
</tbody>
@endforeach

        </table>
      
    </div>
 
 </div>
 <div>
    {{ $category->links() }}
    </div>

 
  @include('admin.script')
  </div>
</body>
</html>







