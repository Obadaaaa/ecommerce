<!DOCTYPE html>
<html lang="en">
<head>
  @include('admin.css')
</head>
<body>
  <div class ="container-scroller">
  @include('admin.navbar')
  @include('admin.sidebar')
 <div class = "main-panel">
    <div class ="content-wrapper">
        @if(session()->has('updated'))

        <div class="alert alert-success">
        {{ session()->get('updated') }}
        </div>

        @endif
   
            <h2> Update Category </h2>
    <form action ="{{ url('admin/Categories/'.$category->id) }}" method ="POST"> 
    @method('PUT')
    @csrf   
    <input type="text" name="name" value ="{{ $category->name }}">
    <input type = "submit" name="sumbit" class = "btn btn-primary" value="update Category">
     </form>

 </div>

  @include('admin.script')
  </div>
</body>
</html>







