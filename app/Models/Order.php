<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
protected $fillable =[
'cart_id',
'user_id',

];

public function users(){
    return $this->belongsTo('App\Models\User','user_id');
}
  public function caa(){
        return $this->belongsTo('App\Models\Cart','cart_id');
    }

    public function products(){
        return $this->belongsTo('App\Models\Product','prod_id');
    }
}
