<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'title',
        'description',
        'image',
        'category',
        'quantity',
        'price',
        'discount_price'];
public function categories (){
    $this->belongsTo('App/Models/category','category_id');
}

public function carts(){
    $this->hasOne('App/Models/Cart','product_id');
}

public function orders(){
    $this->hasOne('App/Models/Order','product_id');
}
}
