<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use HasFactory;
    protected $fillable = ['id','name'];

    public function carts(){
        $this->hasMany('App/Models/Cart','product_id');
    }
}
