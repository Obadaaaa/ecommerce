<?php

namespace App\Http\Controllers;

use App\Models\category;
use Illuminate\Http\Request;

class AdminCategory extends Controller
{

   

    public function index(){
        $category =category::orderBy('id','asc')->paginate(4);
        return view('admin.Categories.index',['category'=>$category]);
    }



    public function create (Request $request){
        $category = new category; 
        $category->name = $request['name'];
$category->save();
return redirect('/admin/Categories')->with('message','Category Added Successfully');
    }

    public function edit ($id){
        $category =category::findOrFail($id);
        return view('admin.Categories.edit',compact('category'));
    }
    public function update (Request $request ,$id){
        $category = category::findOrFail($id);
        $category->name = $request['name'];
        $category->save();
        return redirect('/admin/Categories')->with('updated','Category updated');

    }

    
    public function destory($id){
        category::findOrFail($id)->delete();
        return redirect('/admin/Categories')->with('deleted','Category Deleted');

    }

}
