<?php

namespace App\Http\Controllers;

use App\Models\category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminProducts extends Controller
{


    public function index(){
    $products = Product::all();
    return view('admin.Prodcuts.index',compact('products'));
    }
    public function create(Request $request){
        $category = category::all();
        return view('admin.Prodcuts.create',compact('category'));    }





     public function store (Request $request){
        $product =  new Product;
        $product->title = $request['title'];
        $product->description = $request['description'];
        $product->price =$request['price'];
        $product->discount_price =$request['discount'];
        $product->quantity =$request['quantity'];
        $product->category = $request['category'];


        if ($file = $request->file('image')){
            $name = time().$file->getClientOriginalName();
            $file->move('images',$name);
     $product->image =$name;   }


     $product->save();
 return redirect()->back()->with('message','Product Added succesfully');





     }



     public function update($id,Request $request){


        $product =Product::findOrFail($id);
        $product->title = $request['title'];
        $product->description = $request['description'];
        $product->price =$request['price'];
        $product->discount_price =$request['discount'];
        $product->quantity =$request['quantity'];
        $product->category = $request['category'];


        if ($file = $request->file('image')){
            $name = time().$file->getClientOriginalName();
            $file->move('images',$name);
     $product->image =$name;   }


    $product->update();
 return redirect()->back()->with('message','Product Added succesfully');








     }


     public function edit ($id){
        $product = Product::findOrFail($id);
        $category = category::all();
        return view('admin.Prodcuts.edit',compact('product','category'));

     }
   

     public function destroy ($id){
        $product = Product::findOrFail($id);
        $path ='images/'.$product->image;
       
        if(File::exists($path)){
            File::delete($path);
            
                                }
        $product->delete();
        return redirect()->back();
  
     }

}
