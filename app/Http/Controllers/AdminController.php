<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    

    public function index(){ 
        $total_products =Product::all()->count();
        $total_orders = Order::all()->count();
        $total_users = User::all()->count();
        $order = Order::all();
        $total_revnue = 0;
        foreach($order as $order){
            $total_revnue = $total_revnue  + $order->price;
        }
        $total_deliverd = Order::where('delivery_status','=','deliverd')->get()->count();

        
        return view('admin.dashboard',compact('total_products','total_orders','total_users','total_revnue','total_deliverd'));
    }
}
