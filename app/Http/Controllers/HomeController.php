<?php

namespace App\Http\Controllers;
use App\Models\Cart;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
class   HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    
     
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {$products =Product::paginate(3);
        return view('home.userpage',compact('products'));
     
    }
    public function details($id){
        $product =Product::findOrFail($id);
        return view('home.Details',compact('product'));

    }


    public function add_cart(Request $request ,$id){
     if(Auth::id()){

       $user= Auth::user()->id;
    
 
     $products_exist =Cart::where('product_id','=',$id)->where('user_id','=',$user)->get('id')->first();
     if($products_exist!=null){
        $cart =Cart::findOrFail($products_exist)->first();
        $quantity= $cart->quantity;
        $cart->quantity =$quantity +$request->quantity;
        if($cart->products->discount_price !=null){ 
            $cart->price = $cart->products->discount_price * $cart->quantity; }
            else{
             $cart->price = $cart->products->price * $cart->quantity;
            }
        
        $cart->save();
        return redirect()->back();
     }
     else{

   $cart = new Cart;
   $cart->user_id =$user;
   $cart->product_id = $id;
       $cart->quantity = $request->quantity;
       if($cart->products->discount_price !=null){ 
       $cart->price = $cart->products->discount_price * $cart->quantity; }
       else{
        $cart->price = $cart->products->price * $cart->quantity;
       }
       
       $cart->user_id = Auth::user()->id;
     
    $cart->save();
    return redirect()->back();

}
     }
     else{
        return redirect('login');
     }
    }
    public function showCart(){
        $user = Auth::user()->id;
        $cart = Cart::where('user_id','=',$user)->get();
        $cart->user_id = Auth::user()->id;
        return view('home.showcart',compact('cart'));
    }

    public function deletecart ($id){
        $cart = Cart::findOrFail($id);
        $cart->delete();
        return redirect()->back();

    }

    public function cashorder(){
$user= Auth::user()->id;
$data =Cart::where('user_id','=', $user)->get();  


foreach ($data as $da){
    $order = new Order;  
    $order->user_id = $user;

    $order->quantity = $da->quantity;
    $order->price = $da->price;
$order->payment_status ="cash on Deleivery";
$order->delivery_status ="processing";
$order->prod_id =$da->product_id;
$order->save();
$da->delete();
 }
return redirect()->back()->with('message','we have Recived your Order,we will contact u as soon as possible');


    }
 

    public function ShowOrder (){
        if(Auth::id()){
        $user= Auth::user()->id;
        $order = Order::where('user_id','=',$user)->get();
            return view('home.order',compact('order'));
        }
        else{
            return redirect('login');
        }
    }

public function cancel_order($id){

$order = Order::findOrFail($id);
$order->delivery_status ="Order Cancled";
$order->save();
return redirect()->back();
}
public function search_product (Request $request){
$search = $request->search;
$products = Product::where('title','LIKE',$search)->paginate(3);

return view('home.userpage',compact('products'));
}
public function show_products (){
    $products =Product::paginate(10);

        return view('home.all_products',compact('products'));
}


}
