<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class AdminOrder extends Controller
{
    public function index(){
        $orders = Order::all();
        return view('admin.Orders.index',compact('orders'));
    }
    public function deliverd ($id){
$order= Order::findOrFail($id);
$order->delivery_status ="deliverd";
$order->payment_status ="Paid";
$order->save();
return redirect()->back();
    }
public function printPdf($id){
    $order =Order::findOrFail($id);
 
$pdf = Pdf::loadView('admin.pdf',compact('order'));
return $pdf->download('order_details');
}
public function searchdata(Request $request){
$search= $request['search'];

$user =User::where('name','LIKE',"%$search%")->get();

$user = $user[0]->id;

$orders = Order::where('user_id','LIKE',"%$user%")->get();


return view('admin.Orders.index',compact('orders'));
}
}
